import numpy
import math
import random  # choice mucho mas rapido que en numpy
import operator


def prbs(ammount, symbols):
    signal = []
    for _ in range(ammount):
        signal.append(random.choice(symbols))
    return signal


def noise_add(signal, noise):
    return list(map(operator.add, signal, noise))


def noise_gen(mu, sigma, points):
    return numpy.random.normal(mu, sigma, points)


def avg_power(signal):
    power = [x**2 for x in signal]
    energy = math.sqrt(numpy.mean(power))
    return energy


def avg_energy(signal):
    power = [abs(x)**2 for x in signal]
    energy = sum(power)/len(power)
    return energy


def avg_bit_power(symbols):
    energy = avg_power(symbols)
    bit_count = round(math.log2(len(symbols)))
    return energy/bit_count
