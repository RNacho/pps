import math
import numpy


def serial_filter_fir(bit, regs, coeffs):
    for index in range(len(regs)-1):
        regs[-(index+1)] = regs[-(index+2)]
    regs[0]=bit
    product = [x * y for x,y in zip(regs, coeffs)]
    return [sum(product)]

def filter_fir(signal, coeffs):
    convolution = numpy.convolve(signal, coeffs).tolist()
    center = math.floor(len(coeffs)/2)
    return convolution[center:-center]
