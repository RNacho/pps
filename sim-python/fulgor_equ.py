import math
import scipy.stats as scpss
import fulgor_signal as f_signal
import fulgor_comm as f_comm
import fulgor_theory as f_theory
import fulgor_filter as f_filter

from matplotlib import pyplot as plt

def serial_equalizer(rx_bit, symbols, beta, fir_registers, equ_registers):
    equ_bit = f_filter.serial_filter_fir(rx_bit, fir_registers, equ_registers)

    read_bit = f_comm.slicer(equ_bit, symbols)
    error = read_bit[0] - equ_bit[0]

    equ_registers = [w+(beta*error*x) for x,w in zip(fir_registers,equ_registers)]

    print('error: '+str(error))
    #print(str(rx_bit) + " -> " + str(equ_bit))
    return read_bit, equ_registers

def serial_equ_sim(symbols, snr_db, beta, current_iteration, bit_errors, channel_reg, fir_ch_reg, fir_equ_reg, equ_reg, tx_reg):
    tx_bit = f_signal.prbs(1, symbols)
    for index in range(len(tx_reg)-1):
        tx_reg[-(index+1)] = tx_reg[-(index+2)]
    tx_reg[0]=tx_bit[0]
    
    ch_bit = f_filter.serial_filter_fir(tx_bit[0], fir_ch_reg, channel_reg)
    ns_bit = f_comm.awgn_channel(ch_bit, symbols, snr_db)
    (rx_bit,equ_reg) = serial_equalizer(ns_bit[0], symbols, beta, fir_equ_reg, equ_reg)

    bit_errors += f_comm.error_count([tx_reg[-1]], rx_bit, symbols)
    return ((bit_errors/(current_iteration+1)), bit_errors, equ_reg)