import math
import fulgor_signal as f_signal
import fulgor_filter as f_filter
import fulgor_theory as f_theory


def error_count(signal_a, signal_b, symbols):
    bits_count = round(math.log2(len(symbols)))
    bits_format = "{0:0" + "{}".format(bits_count+1) + "b}"
    bit_error = 0
    for index, value in enumerate(signal_a):
        bits_a = bits_format.format(signal_a[index])
        bits_b = bits_format.format(signal_b[index])
        bit_error += sum(i != j for i, j in zip(bits_a, bits_b))
    return bit_error


def slicer(signal, symbols):
    return [min(symbols, key=lambda x:abs(x-value)) for value in signal]


def awgn_channel(tx_signal, symbols, snr_db):
    signal_energy = f_signal.avg_energy(symbols)
    bit_count = round(math.log2(len(symbols)))
    snr = 10**(snr_db/10) * (1 + ((bit_count-1) * 1))
    sigma = math.sqrt(signal_energy / (2 * snr))

    noise_signal = f_signal.noise_gen(0, sigma, len(tx_signal))
    noisy_signal = f_signal.noise_add(tx_signal, noise_signal)
    return noisy_signal


def ber_calc(tx_signal, symbols, snr_db, min_ber_value=10e-20):
    noisy_signal = awgn_channel(tx_signal, symbols, snr_db)
    rx_signal = slicer(noisy_signal, symbols)

    ber = error_count(tx_signal, rx_signal, symbols)/len(tx_signal)
    return max(ber, min_ber_value)


def parallel_ber_sim(snr_db, symbols, last_ber=None, min_ber=1e-3, min_err_count=1e2):
    if not (last_ber):
        last_ber = 1e-3
    else:
        if(last_ber == min_ber):
            return last_ber
    last_norm_ber = last_ber * len(symbols)/2
    points = round(max(min_err_count//last_norm_ber, 10*min_err_count))
    signal = f_signal.prbs(points, symbols)
    ber = ber_calc(signal, symbols, snr_db, min_ber)
    return ber


def parallel_iq_ber_sim(snr_db, symbols, last_ber, min_ber):
    qam_i_ber = parallel_ber_sim(snr_db, symbols, last_ber, min_ber)
    qam_q_ber = parallel_ber_sim(snr_db, symbols, last_ber, min_ber)
    ber = (qam_i_ber + qam_q_ber)/(len(symbols))
    return ber