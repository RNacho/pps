import numpy
import math
import random  # choice mucho mas rapido que en numpy
import operator
import fulgor_fp as ffp


def fp_prbs(ammount, symbols, quant_params_big, quant_params_small):
    symbols = ffp.quantize_arr(symbols, *quant_params_small)
    signal = []
    for _ in range(ammount):
        signal.append(random.choice(symbols))
    return signal


def fp_noise_add(signal, noise, quant_params_big, quant_params_small):
    signal = ffp.quantize_arr(signal, *quant_params_small)
    noise = ffp.quantize_arr(noise, *quant_params_small)
    noisy = list(map(operator.add, signal, noise)) 
    return ffp.quantize_arr(noisy, *quant_params_small)


def fp_noise_gen(mu, sigma, points, quant_params_big, quant_params_small):
    mu = ffp.quantize("mu", mu, *quant_params_small)
    sigma = ffp.quantize("sigma", sigma, *quant_params_small)
    #points = int(ffp.quantize("points", points, *quant_params_big))
    noise = numpy.random.normal(mu, sigma, points) 
    return ffp.quantize_arr(noise, *quant_params_small)


def fp_avg_power(signal, quant_params_big, quant_params_small):
    signal = ffp.quantize_arr(signal, *quant_params_small)
    power = [x**2 for x in signal]
    power = ffp.quantize_arr(power, *quant_params_big)
    energy = math.sqrt(ffp.quantize("power", numpy.mean(power),*quant_params_big))
    return ffp.quantize("energy", energy, *quant_params_big)


def fp_avg_energy(signal, quant_params_big, quant_params_small):
    signal = ffp.quantize_arr(signal, *quant_params_small)
    power = [abs(x)**2 for x in signal]
    power = ffp.quantize_arr(power, *quant_params_big)
    energy = sum(power)/len(power)
    return ffp.quantize("energy", energy, *quant_params_big)


def fp_avg_bit_power(symbols, quant_params_big, quant_params_small):
    symbols = ffp.quantize_arr(symbols, *quant_params_small)
    energy = avg_power(symbols)
    energy = ffp.quantize("energy", energy, *quant_params_small)
    bit_count = round(math.log2(len(symbols)))
    return ffp.quantize("avg_bit_power", energy/bit_count, *quant_params_small)
