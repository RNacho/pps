import math
import scipy.stats as scpss


def q_func(x):
    return scpss.norm.sf(x)


def bpsk_theory(snr_db):
    snr = 10**(snr_db/10)
    return q_func(math.sqrt((2*snr)))


def qam_theory(snr_db, M=16):
    snr = 10**(snr_db/10)
    b = math.log2(M)
    prod = (4/b) * (1 - 2**(-b/2))
    paren = math.sqrt(3*b*snr / (2**b - 1))
    result = prod * q_func(paren)
    return result
