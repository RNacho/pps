import math
import scipy.stats as scpss
import fulgor_fp_signal as ffp_signal
import fulgor_fp_comm as ffp_comm
import fulgor_fp_filter as ffp_filter
import fulgor_fp as ffp

from matplotlib import pyplot as plt

def fp_serial_equalizer(rx_bit, symbols, beta, fir_registers, equ_registers, quant_params_big, quant_params_small):
    symbols = ffp.quantize_arr(symbols, *quant_params_small)
    beta = ffp.quantize("beta", beta, *quant_params_small)
    rx_bit = ffp.quantize("rx_bit", rx_bit, *quant_params_small)
    #fir_registers = ffp.quantize_arr(fir_registers, *quant_params)
    #equ_registers = ffp.quantize_arr(equ_registers, *quant_params)

    equ_bit = ffp_filter.fp_serial_filter_fir(rx_bit, fir_registers, equ_registers, quant_params_big, quant_params_small)

    read_bit = ffp_comm.fp_slicer(equ_bit, symbols, quant_params_big, quant_params_small)
    error = read_bit[0] - equ_bit[0]

    equ_registers = [w+(beta*error*x) for x,w in zip(fir_registers,equ_registers)]
    equ_registers = ffp.quantize_arr(equ_registers, *quant_params_big)

    print('error: '+str(error))
    #print(str(rx_bit) + " -> " + str(equ_bit))
    return read_bit, equ_registers

def fp_serial_equ_sim(symbols, snr_db, beta, current_iteration, bit_errors, channel_reg, fir_ch_reg, fir_equ_reg, equ_reg, tx_reg, quant_params_big, quant_params_small):
    symbols = ffp.quantize_arr(symbols, *quant_params_small)
    snr_db = ffp.quantize("snr_db", snr_db, *quant_params_big)
    beta = ffp.quantize("beta", beta, *quant_params_small)
    channel_reg = ffp.quantize_arr(channel_reg, *quant_params)
    #fir_ch_reg = ffp.quantize_arr(fir_ch_reg, *quant_params)
    #fir_equ_reg = ffp.quantize_arr(fir_equ_reg, *quant_params)
    #equ_reg = ffp.quantize_arr(equ_reg, *quant_params)
    #tx_reg = ffp.quantize_arr(tx_reg, *quant_params)

    tx_bit = ffp_signal.fp_prbs(1, symbols, quant_params_big, quant_params_small)
    for index in range(len(tx_reg)-1):
        tx_reg[-(index+1)] = tx_reg[-(index+2)]
    tx_reg[0]=tx_bit[0]
    
    ch_bit = ffp_filter.fp_serial_filter_fir(tx_bit[0], fir_ch_reg, channel_reg, quant_params_big, quant_params_small)
    ns_bit = ffp_comm.fp_awgn_channel(ch_bit, symbols, snr_db, quant_params_big, quant_params_small)
    (rx_bit,equ_reg) = fp_serial_equalizer(ns_bit[0], symbols, beta, fir_equ_reg, equ_reg, quant_params_big, quant_params_small)

    bit_errors += ffp_comm.fp_error_count([tx_reg[-1]], rx_bit, symbols, quant_params_big, quant_params_small)
    return ((bit_errors/(current_iteration+1)), bit_errors, equ_reg)