import numpy
from tool._fixedInt import *

debug_fp = True

def quantize(name, value, nb, nbf, sign, rounding, saturation):
    q = DeFixedInt(nb, nbf, sign, rounding, saturation) 
    if not(value):
        return value
    q.value = float(value)
    if (debug_fp):
        print("Quant error in {}({}) with ({}{},{}): {}".format(name, value, sign ,nb, nbf, (q.fValue - value)))
    return q.fValue

def quantize_arr(values, nb, nbf, sign, rounding, saturation):
    q_arr = arrayFixedInt(nb, nbf, numpy.array([float(x) for x in values]), sign, rounding, saturation)
    f_q_arr = [x.fValue for x in q_arr]
    return f_q_arr
