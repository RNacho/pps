#!/usr/bin/env python3

import numpy
import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
import fulgor_fp_comm as ffp_comm
import fulgor_theory as f_theory
import fulgor_fp as ffp

NB_big = 24
NBF_big = 8
NB_small = 24
NBF_small = 16
SRS_global = ('S', 'trunc', 'saturate')
Parameters_big = (NB_big, NBF_big, *SRS_global)
Parameters_small = (NB_small, NBF_small, *SRS_global)

mplstyle.use(['ggplot', 'fast'])

symbols = [1, -1, 3, -3, 5, -5, 7, -7]  # Must be in increasing magnitude

bpsk_symbols = symbols[0:2]
qpsk_symbols = symbols[0:2]
qam16_symbols = symbols[0:4]
qam64_symbols = symbols[0:8]

resolution = 0.25
snr_db_limit = 15

snr_list = list(numpy.arange(0, snr_db_limit, resolution))
bpsk_ber = [None]*len(snr_list)
qpsk_ber = [None]*len(snr_list)
qam16_ber = [None]*len(snr_list)
qam64_ber = [None]*len(snr_list)

min_ber = 5e-7
min_data = 1e3

keep_plot = True


def handle_close(evt):
    global keep_plot
    keep_plot = False


plt.figure().canvas.mpl_connect('close_event', handle_close)

for index, snr_db in enumerate(snr_list):
    if(keep_plot):

        if(index < 9 or (bpsk_ber[index-1] and bpsk_ber[index-1] > min_ber/1)):
            bpsk_ber[index] = ffp_comm.fp_parallel_ber_sim(
                snr_db, bpsk_symbols, bpsk_ber[index-1], min_ber, min_data, Parameters_big, Parameters_small)
        if(index < 9 or (qpsk_ber[index-1] and qpsk_ber[index-1] > min_ber/1)):
            qpsk_ber[index] = ffp_comm.fp_parallel_iq_ber_sim(
                snr_db, qpsk_symbols, qpsk_ber[index-1], min_ber, min_data, Parameters_big, Parameters_small)
        if(index < 9 or (qam16_ber[index-1] and qam16_ber[index-1] > min_ber/2)):
            qam16_ber[index] = ffp_comm.fp_parallel_iq_ber_sim(
                snr_db, qam16_symbols, qam16_ber[index-1], min_ber, min_data, Parameters_big, Parameters_small)
        if(index < 9 or (qam64_ber[index-1] and qam64_ber[index-1] > min_ber/4)):
            qam64_ber[index] = ffp_comm.fp_parallel_iq_ber_sim(
                snr_db, qam64_symbols, qam64_ber[index-1], min_ber, min_data, Parameters_big, Parameters_small)

        plt.cla()
        plt.xlabel("SNR [dB]")
        plt.ylabel("BER [-]")
        plt.yscale('log')
        plt.grid(True, 'both', 'both')
        plt.ion()

        plt.plot(snr_list[0:index], bpsk_ber[0:index], 'g--', label='BPSK Sim')
        plt.plot(snr_list[0:index], qpsk_ber[0:index], 'y--', label='QPSK Sim')
        plt.plot(snr_list[0:index], qam16_ber[0:index],
                 'r--', label='QAM16 Sim')
        plt.plot(snr_list[0:index], qam64_ber[0:index],
                 'k--', label='QAM64 Sim')

        plt.plot(snr_list[0:index], [f_theory.bpsk_theory(x)
                                     for x in snr_list[0:index]], 'm-', label='BPSK Theory')
        plt.plot(snr_list[0:index], [f_theory.qam_theory(x, 16)
                                     for x in snr_list[0:index]], 'b-', label='QAM16 Theory')
        plt.plot(snr_list[0:index], [f_theory.qam_theory(x, 64)
                                     for x in snr_list[0:index]], 'g-', label='QAM64 Theory')

        plt.legend()
        plt.pause(10e-2)

if(keep_plot):
    print("Finished plotting!")
    plt.ioff()
    plt.show()
