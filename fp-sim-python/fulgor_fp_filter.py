import math
import numpy
import fulgor_fp as ffp


def fp_serial_filter_fir(bit, regs, coeffs, quant_params_big, quant_params_small):
    bit = ffp.quantize("bit", bit, *quant_params_small)
    coeffs = ffp.quantize_arr(coeffs, *quant_params_big)

    for index in range(len(regs)-1):
        regs[-(index+1)] = regs[-(index+2)]
    regs[0] = bit
    product = [x * y for x,y in zip(regs, coeffs)]
    product = ffp.quantize_arr(product, *quant_params_big)
    qsum = ffp.quantize("qsum", sum(product), *quant_params_big)
    return [qsum]

def fp_filter_fir(signal, coeffs, quant_params_big, quant_params_small):
    signal = ffp.quantize_arr(signal, *quant_params_small)
    coeffs = ffp.quantize_arr(coeffs, *quant_params_big)
    convolution = numpy.convolve(signal, coeffs).tolist()
    convolution = ffp.quantize_arr(convolution, *quant_params_big)
    center = math.floor(len(coeffs)/2)
    return convolution[center:-center]
