#!/usr/bin/env python3

import numpy
import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
import fulgor_fp_equ as ffp_equ

mplstyle.use(['ggplot', 'fast'])

symbols = [1, -1, 3, -3, 5, -5, 7, -7]  # Must be in increasing magnitude

bpsk_symbols = symbols[0:2]
qpsk_symbols = symbols[0:2]
qam16_symbols = symbols[0:4]
qam64_symbols = symbols[0:8]

NB_big = 32
NBF_big = 24
NB_small = 32
NBF_small = 8
SRS_global = ('S', 'trunc', 'saturate')
Parameters_big = (NB_big, NBF_big, *SRS_global)
Parameters_small = (NB_small, NBF_small, *SRS_global)


keep_plot = True


def handle_close(evt):
    global keep_plot
    keep_plot = False

plt.figure().canvas.mpl_connect('close_event', handle_close)

channel_reg = [0.1, 0.2, 0.6, 1, 0, 0, 0]
filter_size = len(channel_reg)
equ_reg = [0]*(filter_size//2) + [1] + [0]*(filter_size//2)
fir_ch_reg = [0]*filter_size
fir_equ_reg = [0]*filter_size
tx_reg = [0]*filter_size
bit_errors=0
snr_db = 1e1
beta = 0.02

iterations = 100000
for index in range(iterations):
    if(keep_plot):
        plt.cla()
        plt.xlabel("Digital Frequency")
        plt.ylabel("Coeff value")
        plt.yscale('linear')
        plt.grid(True, 'both', 'both')
        plt.ion()

        ber_equ, bit_errors, equ_reg = ffp_equ.fp_serial_equ_sim(
            bpsk_symbols, snr_db, beta, index, bit_errors, channel_reg, fir_ch_reg, fir_equ_reg, equ_reg, tx_reg, Parameters_big, Parameters_small)
        plotlabel = 'Equ coeffs. iteration: ' + str(index) + '\nBER: ' + str(ber_equ) + '\nError count: ' + str(bit_errors)
        plt.plot([-3,-2,-1,0,1,2,3], equ_reg,'b', label=plotlabel)
        plt.stem([-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6], numpy.convolve(channel_reg, equ_reg).tolist(), label='Ch conv Equ', use_line_collection=True)

        plt.legend()
        plt.pause(1e-3)

if(keep_plot):
    print("Finished plotting!")
    plt.ioff()
    plt.show()