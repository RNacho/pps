import math
import fulgor_fp_signal as ffp_signal
import fulgor_fp_filter as ffp_filter
import fulgor_theory as f_theory
import fulgor_fp as ffp


def fp_error_count(signal_a, signal_b, symbols, quant_params_big, quant_params_small):
    signal_a = ffp.quantize_arr(signal_a, *quant_params_small)
    signal_b = ffp.quantize_arr(signal_b, *quant_params_small)
    symbols = ffp.quantize_arr(symbols, *quant_params_small)
    bits_count = round(math.log2(len(symbols)))
    bits_format = "{0:0" + "{}".format(bits_count+1) + "b}"
    bit_error = 0
    for index, value in enumerate(signal_b):
        bits_a = bits_format.format(int(signal_a[index]))
        bits_b = bits_format.format(int(signal_b[index]))
        bit_error += sum(i != j for i, j in zip(bits_a, bits_b))
    return bit_error


def fp_slicer(signal, symbols, quant_params_big, quant_params_small):
    signal = ffp.quantize_arr(signal, *quant_params_small)
    symbols = ffp.quantize_arr(symbols, *quant_params_small)
    return [min(symbols, key=lambda x:abs(x-value)) for value in signal]


def fp_awgn_channel(tx_signal, symbols, snr_db, quant_params_big, quant_params_small):
    tx_signal = ffp.quantize_arr(tx_signal, *quant_params_small)
    symbols = ffp.quantize_arr(symbols, *quant_params_small)
    snr_db = ffp.quantize("snr_db", snr_db, *quant_params_big)

    signal_energy = ffp_signal.fp_avg_energy(symbols, quant_params_big, quant_params_small)
    bit_count = round(math.log2(len(symbols)))
    snr = 10**(snr_db/10) * (1 + ((bit_count-1) * 1))
    sigma = math.sqrt(signal_energy / (2 * snr))

    noise_signal = ffp_signal.fp_noise_gen(0, sigma, len(tx_signal), quant_params_big, quant_params_small)
    noisy_signal = ffp_signal.fp_noise_add(tx_signal, noise_signal, quant_params_big, quant_params_small)
    return noisy_signal


def fp_ber_calc(tx_signal, symbols, snr_db, min_ber_value, quant_params_big, quant_params_small):
    tx_signal = ffp.quantize_arr(tx_signal, *quant_params_small)
    symbols = ffp.quantize_arr(symbols, *quant_params_small)
    snr_db = ffp.quantize("snr_db", snr_db, *quant_params_big)

    noisy_signal = fp_awgn_channel(tx_signal, symbols, snr_db, quant_params_big, quant_params_small)
    rx_signal = fp_slicer(noisy_signal, symbols, quant_params_big, quant_params_small)

    ber = fp_error_count(tx_signal, rx_signal, symbols, quant_params_big, quant_params_small)/len(tx_signal)
    return ffp.quantize("ber", max(ber, min_ber_value), *quant_params_small)


def fp_parallel_ber_sim(snr_db, symbols, last_ber, min_ber, min_err_count, quant_params_big, quant_params_small):
    snr_db = ffp.quantize("snr_db", snr_db, *quant_params_big)
    symbols = ffp.quantize_arr(symbols, *quant_params_small)
    last_ber = ffp.quantize("last_ber", last_ber, *quant_params_small)

    if not (last_ber):
        last_ber = 1e-3
    else:
        if(last_ber == min_ber):
            return last_ber
    last_norm_ber = ffp.quantize("last_norm_ber", last_ber * len(symbols)/2, *quant_params_small)
    points = round(max(min_err_count//(last_norm_ber), 10*min_err_count))
    signal = ffp_signal.fp_prbs(points, symbols, quant_params_big, quant_params_small)
    ber = fp_ber_calc(signal, symbols, snr_db, min_ber, quant_params_big, quant_params_small)
    return ber


def fp_parallel_iq_ber_sim(snr_db, symbols, last_ber, min_ber, min_data, quant_params_big, quant_params_small):
    snr_db = ffp.quantize("snr_db", snr_db, *quant_params_big)
    symbols = ffp.quantize_arr(symbols, *quant_params_small)
    last_ber = ffp.quantize("last_ber", last_ber, *quant_params_small)

    qam_i_ber = fp_parallel_ber_sim(snr_db, symbols, last_ber, min_ber, min_data, quant_params_big, quant_params_small)
    qam_q_ber = fp_parallel_ber_sim(snr_db, symbols, last_ber, min_ber, min_data, quant_params_big, quant_params_small)
    ber = ffp.quantize("ber", (qam_i_ber + qam_q_ber)/(len(symbols)), *quant_params_small)
    return ber