class monitor_prbs #(parameter OUTPUT_NB, parameter LENGTH);
   virtual if_prbs #(.OUTPUT_NB(OUTPUT_NB), .LENGTH(LENGTH)) vif;
   mailbox scb_mbx;

   task run();
      $display("%10t [MONITR] Starting", $time);

      forever begin
         trans_prbs #(.OUTPUT_NB(OUTPUT_NB), .LENGTH(LENGTH)) trans = new;
         @(posedge vif.clk);
         trans.i_seed = vif.i_seed;
         trans.o_data = vif.o_data;
         trans.print("MONITR");
         scb_mbx.put(trans);
      end
   endtask // run

endclass // monitor_prbs
