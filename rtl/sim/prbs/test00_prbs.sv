`include "env_prbs.sv"

class test00_prbs #(parameter OUTPUT_NB, parameter LENGTH);
   env_prbs #(.OUTPUT_NB(OUTPUT_NB), .LENGTH(LENGTH)) e0;
   mailbox drv_mbx;

   function new();
      drv_mbx = new();
      e0 = new();
   endfunction // new
   
   virtual task run();
      e0.d0.drv_mbx = drv_mbx;

      fork
         e0.run();
      join_none

      apply_stim();
   endtask // run

   virtual task apply_stim();
      trans_prbs #(.OUTPUT_NB(OUTPUT_NB), .LENGTH(LENGTH)) trans;

      $display("%10t [TEST00] Starting stimulus", $time);
      trans = new;
      trans.randomize() with {i_seed != ((1<<LENGTH)-1);};
      drv_mbx.put(trans);
   endtask // apply_stim

endclass // test00_prbs