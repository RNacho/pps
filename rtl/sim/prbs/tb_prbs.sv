`include "test00_prbs.sv"

module tb_prbs;
   parameter OUTPUT_NB = 24;
   parameter LENGTH = 64;
   reg clk;

   always #10 clk = ~clk;
   if_prbs #(.OUTPUT_NB(OUTPUT_NB), .LENGTH(LENGTH)) _if(clk);

   prbs #(.OUTPUT_NB(OUTPUT_NB), .LFSR_LENGTH(LENGTH))
   DUT_prbs(
            .o_data(_if.o_data),
            .i_seed(_if.i_seed),
            .rst(_if.rst),
            .clk(clk)
            );

   initial begin
      test00_prbs #(.OUTPUT_NB(OUTPUT_NB), .LENGTH(LENGTH)) t0;

      clk <= 0;
      _if.rst <= 1;
      _if.i_seed <= 132341;
      #20 _if.rst <= 0;

      t0 = new;
      t0.e0.vif = _if;
      t0.run();

      #200 $finish;
   end // initial begin
   
   initial begin
      $dumpfile("dump.vcd");
      $dumpvars;
   end

endmodule // tb_prbs