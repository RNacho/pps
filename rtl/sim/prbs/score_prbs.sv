class score_prbs #(parameter OUTPUT_NB, parameter LENGTH);
   mailbox scb_mbx;
   trans_prbs #(.OUTPUT_NB(OUTPUT_NB), .LENGTH(LENGTH)) transq[3];
   int     i = 0;
   int repetition = 0;

   task run();
      forever begin
         trans_prbs #(.OUTPUT_NB(OUTPUT_NB), .LENGTH(LENGTH)) trans;
         scb_mbx.get(trans);
         //trans.print("SCRBRD");

         transq[1] = transq[0];
         transq[0] = trans;
         i++;
         if(transq[1])
           if(transq[1].o_data == transq[0].o_data) begin
              repetition++;
              $display("%10t [SCRBRD] WARNING: %0d consecutive equal values. Iteration %0d", $time, repetition, i);
           end
           else repetition = 0;
      end
   endtask // run

endclass // score_prbs