interface if_prbs #(parameter OUTPUT_NB, parameter LENGTH) (input bit clk);
   logic rst;
   logic [OUTPUT_NB-1:0] o_data;
   logic [LENGTH-1:0] i_seed;
endinterface // if_prbs