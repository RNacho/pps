`include "driver_prbs.sv"
`include "monitor_prbs.sv"
`include "score_prbs.sv"

class env_prbs #(parameter OUTPUT_NB, parameter LENGTH);
   driver_prbs #(.OUTPUT_NB(OUTPUT_NB), .LENGTH(LENGTH)) d0;
   monitor_prbs #(.OUTPUT_NB(OUTPUT_NB), .LENGTH(LENGTH)) m0;
   score_prbs #(.OUTPUT_NB(OUTPUT_NB), .LENGTH(LENGTH)) s0;
   mailbox scb_mbx;
   virtual if_prbs #(.OUTPUT_NB(OUTPUT_NB), .LENGTH(LENGTH)) vif;

   function new();
      d0 = new;
      m0 = new;
      s0 = new;
      scb_mbx = new();
   endfunction // new

   virtual task run();
      d0.vif = vif;
      m0.vif = vif;
      m0.scb_mbx = scb_mbx;
      s0.scb_mbx = scb_mbx;

      fork
         s0.run();
         d0.run();
         m0.run();
      join_any
   endtask // run

endclass // env_prbs