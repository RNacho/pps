module tb_simple_lfsr;
   parameter LENGTH = 37;

   logic clk, rst;
   logic [LENGTH-1:0] o_data, i_seed;

   always #10 clk = ~clk;
   lfsr #(LENGTH)
   DUT_lfsr(
            .o_data(o_data),
            .i_seed(i_seed),
            .rst(rst),
            .clk(clk)
            );

   initial begin
      clk <= 0;
      rst <= 1;
      i_seed <= 132341;
      #20 rst <= 0;
   end // initial begin
   
endmodule // tb_lfsr