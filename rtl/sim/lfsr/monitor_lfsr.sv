class monitor_lfsr #(parameter LENGTH);
   virtual if_lfsr#(LENGTH) vif;
   mailbox scb_mbx;

   task run();
      $display("%10t [MONITR] Starting", $time);

      forever begin
         trans_lfsr#(LENGTH) trans = new;
         @(posedge vif.clk);
         trans.i_seed = vif.i_seed;
         trans.o_data = vif.o_data;
         trans.print("MONITR");
         scb_mbx.put(trans);
      end
   endtask // run

endclass // monitor_lfsr
