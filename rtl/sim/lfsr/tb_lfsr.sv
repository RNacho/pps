`include "test00_lfsr.sv"

module tb_lfsr;
   parameter LENGTH = 3;
   reg clk;

   always #10 clk = ~clk;
   if_lfsr#(LENGTH) _if(clk);

   lfsr #(LENGTH)
   DUT_lfsr(
            .o_data(_if.o_data),
            .i_seed(_if.i_seed),
            .rst(_if.rst),
            .clk(clk)
            );

   initial begin
      test00_lfsr#(LENGTH) t0;

      clk <= 0;
      _if.rst <= 1;
      _if.i_seed <= 132341;
      #20 _if.rst <= 0;

      t0 = new;
      t0.e0.vif = _if;
      t0.run();

      #200 $finish;
   end // initial begin
   
   initial begin
      $dumpfile("dump.vcd");
      $dumpvars;
   end

endmodule // tb_lfsr