`include "env_lfsr.sv"

class test00_lfsr #(parameter LENGTH);
   env_lfsr#(LENGTH) e0;
   mailbox drv_mbx;

   function new();
      drv_mbx = new();
      e0 = new();
   endfunction // new
   
   virtual task run();
      e0.d0.drv_mbx = drv_mbx;

      fork
         e0.run();
      join_none

      apply_stim();
   endtask // run

   virtual task apply_stim();
      trans_lfsr#(LENGTH) trans;

      $display("%10t [TEST00] Starting stimulus", $time);
      trans = new;
      trans.randomize() with {i_seed != ((1<<LENGTH)-1);};
      drv_mbx.put(trans);
   endtask // apply_stim

endclass // test00_lfsr