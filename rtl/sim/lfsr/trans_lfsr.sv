class trans_lfsr #(parameter LENGTH);
   bit [LENGTH-1:0] o_data;
   rand bit [LENGTH-1:0] i_seed;

   function void print(string tag="");
      $display("%10t [%s] Seed: 0x%0h Data: 0x%0h",
               $time, tag, i_seed, o_data);
   endfunction // print

endclass // trans