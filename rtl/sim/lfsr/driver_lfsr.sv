`include "trans_lfsr.sv"

class  driver_lfsr #(parameter LENGTH);
   virtual if_lfsr#(LENGTH) vif;
   event   drv_done;
   mailbox drv_mbx;

   task run();
      $display ("%10t [DRIVER] Starting", $time);
      @(posedge vif.clk);

      forever begin
         trans_lfsr#(LENGTH) trans;

         $display("%10t [DRIVER] Waiting for transaction", $time);
         drv_mbx.get(trans);
         trans.print("DRIVER");
         vif.i_seed <= trans.i_seed;
         @(posedge vif.clk);
         ->drv_done;
      end
   endtask // run
endclass // driver_lfsr