class score_lfsr #(parameter LENGTH);
   mailbox scb_mbx;
   trans_lfsr#(LENGTH) transq[3];

   task run();
      forever begin
         trans_lfsr#(LENGTH) trans;
         scb_mbx.get(trans);
         //trans.print("SCRBRD");

         transq[0] = trans;
         transq[1] = transq[0];
         transq[2] = transq[1];
         if(transq[2] == transq[0])
           $display("%10t [SCRBRD] ERROR: Consecutive equal values. Seed: 0x%0h Data: 0x%0h",
                    $time, trans.i_seed, trans.o_data);
      end
   endtask // run

endclass // score_lfsr