`include "driver_lfsr.sv"
`include "monitor_lfsr.sv"
`include "score_lfsr.sv"

class env_lfsr #(parameter LENGTH);
   driver_lfsr #(LENGTH) d0;
   monitor_lfsr #(LENGTH) m0;
   score_lfsr #(LENGTH) s0;
   mailbox scb_mbx;
   virtual if_lfsr#(LENGTH) vif;

   function new();
      d0 = new;
      m0 = new;
      s0 = new;
      scb_mbx = new();
   endfunction // new

   virtual task run();
      d0.vif = vif;
      m0.vif = vif;
      m0.scb_mbx = scb_mbx;
      s0.scb_mbx = scb_mbx;

      fork
         s0.run();
         d0.run();
         m0.run();
      join_any
   endtask // run

endclass // env