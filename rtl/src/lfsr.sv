
module lfsr
  #(
    parameter LENGTH
    )(
      output [LENGTH-1:0] o_data,
      input [LENGTH-1:0]  i_seed,
      input               rst,
      input               clk
      );

   logic [LENGTH-1:0]     l_data;
   logic                  l_xnor;

   always @(*)
     begin
        case (LENGTH)
          // http://www.xilinx.com/support/documentation/application_notes/xapp052.pdf
          003: l_xnor = l_data[002] ^~ l_data[001];
          004: l_xnor = l_data[003] ^~ l_data[002];
          005: l_xnor = l_data[004] ^~ l_data[002];
          006: l_xnor = l_data[005] ^~ l_data[004];
          007: l_xnor = l_data[006] ^~ l_data[005];
          008: l_xnor = l_data[007] ^~ l_data[005] ^~ l_data[004] ^~ l_data[003];
          009: l_xnor = l_data[008] ^~ l_data[004];
          010: l_xnor = l_data[009] ^~ l_data[006];
          011: l_xnor = l_data[010] ^~ l_data[008];
          012: l_xnor = l_data[011] ^~ l_data[005] ^~ l_data[003] ^~ l_data[000];
          013: l_xnor = l_data[012] ^~ l_data[003] ^~ l_data[002] ^~ l_data[000];
          014: l_xnor = l_data[013] ^~ l_data[004] ^~ l_data[002] ^~ l_data[000];
          015: l_xnor = l_data[014] ^~ l_data[013];
          016: l_xnor = l_data[015] ^~ l_data[014] ^~ l_data[012] ^~ l_data[003];
          017: l_xnor = l_data[016] ^~ l_data[013];
          018: l_xnor = l_data[017] ^~ l_data[010];
          019: l_xnor = l_data[018] ^~ l_data[005] ^~ l_data[001] ^~ l_data[000];
          020: l_xnor = l_data[019] ^~ l_data[016];
          021: l_xnor = l_data[020] ^~ l_data[018];
          022: l_xnor = l_data[021] ^~ l_data[020];
          023: l_xnor = l_data[022] ^~ l_data[017];
          024: l_xnor = l_data[023] ^~ l_data[022] ^~ l_data[021] ^~ l_data[016];
          025: l_xnor = l_data[024] ^~ l_data[021];
          026: l_xnor = l_data[025] ^~ l_data[005] ^~ l_data[001] ^~ l_data[000];
          027: l_xnor = l_data[026] ^~ l_data[004] ^~ l_data[001] ^~ l_data[000];
          028: l_xnor = l_data[027] ^~ l_data[024];
          029: l_xnor = l_data[028] ^~ l_data[026];
          030: l_xnor = l_data[029] ^~ l_data[005] ^~ l_data[003] ^~ l_data[000];
          031: l_xnor = l_data[030] ^~ l_data[027];
          032: l_xnor = l_data[031] ^~ l_data[021] ^~ l_data[001] ^~ l_data[000];
          033: l_xnor = l_data[032] ^~ l_data[019];
          034: l_xnor = l_data[033] ^~ l_data[026] ^~ l_data[001] ^~ l_data[000];
          035: l_xnor = l_data[034] ^~ l_data[032];
          036: l_xnor = l_data[035] ^~ l_data[024];
          037: l_xnor = l_data[036] ^~ l_data[004] ^~ l_data[003] ^~ l_data[002] ^~ l_data[001] ^~ l_data[000];
          038: l_xnor = l_data[037] ^~ l_data[005] ^~ l_data[004] ^~ l_data[000];
          039: l_xnor = l_data[038] ^~ l_data[034];
          040: l_xnor = l_data[039] ^~ l_data[037] ^~ l_data[020] ^~ l_data[018];
          041: l_xnor = l_data[040] ^~ l_data[037];
          042: l_xnor = l_data[041] ^~ l_data[040] ^~ l_data[019] ^~ l_data[018];
          043: l_xnor = l_data[042] ^~ l_data[041] ^~ l_data[037] ^~ l_data[036];
          044: l_xnor = l_data[043] ^~ l_data[042] ^~ l_data[017] ^~ l_data[016];
          045: l_xnor = l_data[044] ^~ l_data[043] ^~ l_data[041] ^~ l_data[040];
          046: l_xnor = l_data[045] ^~ l_data[044] ^~ l_data[025] ^~ l_data[024];
          047: l_xnor = l_data[046] ^~ l_data[041];
          048: l_xnor = l_data[047] ^~ l_data[046] ^~ l_data[020] ^~ l_data[019];
          049: l_xnor = l_data[048] ^~ l_data[039];
          050: l_xnor = l_data[049] ^~ l_data[048] ^~ l_data[023] ^~ l_data[022];
          051: l_xnor = l_data[050] ^~ l_data[049] ^~ l_data[035] ^~ l_data[034];
          052: l_xnor = l_data[051] ^~ l_data[048];
          053: l_xnor = l_data[052] ^~ l_data[051] ^~ l_data[037] ^~ l_data[036];
          054: l_xnor = l_data[053] ^~ l_data[052] ^~ l_data[017] ^~ l_data[016];
          055: l_xnor = l_data[054] ^~ l_data[030];
          056: l_xnor = l_data[055] ^~ l_data[054] ^~ l_data[034] ^~ l_data[033];
          057: l_xnor = l_data[056] ^~ l_data[049];
          058: l_xnor = l_data[057] ^~ l_data[038];
          059: l_xnor = l_data[058] ^~ l_data[057] ^~ l_data[037] ^~ l_data[036];
          060: l_xnor = l_data[059] ^~ l_data[058];
          061: l_xnor = l_data[060] ^~ l_data[059] ^~ l_data[045] ^~ l_data[044];
          062: l_xnor = l_data[061] ^~ l_data[060] ^~ l_data[005] ^~ l_data[004];
          063: l_xnor = l_data[062] ^~ l_data[061];
          064: l_xnor = l_data[063] ^~ l_data[062] ^~ l_data[060] ^~ l_data[059];
          065: l_xnor = l_data[064] ^~ l_data[046];
          066: l_xnor = l_data[065] ^~ l_data[064] ^~ l_data[056] ^~ l_data[055];
          067: l_xnor = l_data[066] ^~ l_data[065] ^~ l_data[057] ^~ l_data[056];
          068: l_xnor = l_data[067] ^~ l_data[058];
          069: l_xnor = l_data[068] ^~ l_data[066] ^~ l_data[041] ^~ l_data[039];
          070: l_xnor = l_data[069] ^~ l_data[068] ^~ l_data[054] ^~ l_data[053];
          071: l_xnor = l_data[070] ^~ l_data[064];
          072: l_xnor = l_data[071] ^~ l_data[065] ^~ l_data[024] ^~ l_data[018];
          073: l_xnor = l_data[072] ^~ l_data[047];
          074: l_xnor = l_data[073] ^~ l_data[072] ^~ l_data[058] ^~ l_data[057];
          075: l_xnor = l_data[074] ^~ l_data[073] ^~ l_data[064] ^~ l_data[063];
          076: l_xnor = l_data[075] ^~ l_data[074] ^~ l_data[040] ^~ l_data[039];
          077: l_xnor = l_data[076] ^~ l_data[075] ^~ l_data[046] ^~ l_data[045];
          078: l_xnor = l_data[077] ^~ l_data[076] ^~ l_data[058] ^~ l_data[057];
          079: l_xnor = l_data[078] ^~ l_data[076];
          080: l_xnor = l_data[079] ^~ l_data[078] ^~ l_data[042] ^~ l_data[041];
          081: l_xnor = l_data[080] ^~ l_data[076];
          082: l_xnor = l_data[081] ^~ l_data[078] ^~ l_data[046] ^~ l_data[043];
          083: l_xnor = l_data[082] ^~ l_data[081] ^~ l_data[037] ^~ l_data[036];
          084: l_xnor = l_data[083] ^~ l_data[070];
          085: l_xnor = l_data[084] ^~ l_data[083] ^~ l_data[057] ^~ l_data[056];
          086: l_xnor = l_data[085] ^~ l_data[084] ^~ l_data[073] ^~ l_data[072];
          087: l_xnor = l_data[086] ^~ l_data[073];
          088: l_xnor = l_data[087] ^~ l_data[086] ^~ l_data[016] ^~ l_data[015];
          089: l_xnor = l_data[088] ^~ l_data[050];
          090: l_xnor = l_data[089] ^~ l_data[088] ^~ l_data[071] ^~ l_data[070];
          091: l_xnor = l_data[090] ^~ l_data[089] ^~ l_data[007] ^~ l_data[006];
          092: l_xnor = l_data[091] ^~ l_data[090] ^~ l_data[079] ^~ l_data[078];
          093: l_xnor = l_data[092] ^~ l_data[090];
          094: l_xnor = l_data[093] ^~ l_data[072];
          095: l_xnor = l_data[094] ^~ l_data[083];
          096: l_xnor = l_data[095] ^~ l_data[093] ^~ l_data[048] ^~ l_data[046];
          097: l_xnor = l_data[096] ^~ l_data[090];
          098: l_xnor = l_data[097] ^~ l_data[086];
          099: l_xnor = l_data[098] ^~ l_data[096] ^~ l_data[053] ^~ l_data[051];
          100: l_xnor = l_data[99] ^~ l_data[062];
          101: l_xnor = l_data[100] ^~ l_data[99] ^~ l_data[094] ^~ l_data[093];
          102: l_xnor = l_data[101] ^~ l_data[100] ^~ l_data[035] ^~ l_data[034];
          103: l_xnor = l_data[102] ^~ l_data[093];
          104: l_xnor = l_data[103] ^~ l_data[102] ^~ l_data[093] ^~ l_data[092];
          105: l_xnor = l_data[104] ^~ l_data[088];
          106: l_xnor = l_data[105] ^~ l_data[090];
          107: l_xnor = l_data[106] ^~ l_data[104] ^~ l_data[043] ^~ l_data[041];
          108: l_xnor = l_data[107] ^~ l_data[076];
          109: l_xnor = l_data[108] ^~ l_data[107] ^~ l_data[102] ^~ l_data[101];
          110: l_xnor = l_data[109] ^~ l_data[108] ^~ l_data[097] ^~ l_data[096];
          111: l_xnor = l_data[110] ^~ l_data[100];
          112: l_xnor = l_data[111] ^~ l_data[109] ^~ l_data[068] ^~ l_data[066];
          113: l_xnor = l_data[112] ^~ l_data[103];
          114: l_xnor = l_data[113] ^~ l_data[112] ^~ l_data[032] ^~ l_data[031];
          115: l_xnor = l_data[114] ^~ l_data[113] ^~ l_data[100] ^~ l_data[99];
          116: l_xnor = l_data[115] ^~ l_data[114] ^~ l_data[045] ^~ l_data[044];
          117: l_xnor = l_data[116] ^~ l_data[114] ^~ l_data[098] ^~ l_data[096];
          118: l_xnor = l_data[117] ^~ l_data[084];
          119: l_xnor = l_data[118] ^~ l_data[110];
          120: l_xnor = l_data[119] ^~ l_data[112] ^~ l_data[008] ^~ l_data[001];
          121: l_xnor = l_data[120] ^~ l_data[102];
          122: l_xnor = l_data[121] ^~ l_data[120] ^~ l_data[062] ^~ l_data[061];
          123: l_xnor = l_data[122] ^~ l_data[120];
          124: l_xnor = l_data[123] ^~ l_data[086];
          125: l_xnor = l_data[124] ^~ l_data[123] ^~ l_data[017] ^~ l_data[016];
          126: l_xnor = l_data[125] ^~ l_data[124] ^~ l_data[089] ^~ l_data[088];
          127: l_xnor = l_data[126] ^~ l_data[125];
          128: l_xnor = l_data[127] ^~ l_data[125] ^~ l_data[100] ^~ l_data[098];
          129: l_xnor = l_data[128] ^~ l_data[123];
          130: l_xnor = l_data[129] ^~ l_data[126];
          131: l_xnor = l_data[130] ^~ l_data[129] ^~ l_data[083] ^~ l_data[082];
          132: l_xnor = l_data[131] ^~ l_data[102];
          133: l_xnor = l_data[132] ^~ l_data[131] ^~ l_data[081] ^~ l_data[080];
          134: l_xnor = l_data[133] ^~ l_data[076];
          135: l_xnor = l_data[134] ^~ l_data[123];
          136: l_xnor = l_data[135] ^~ l_data[134] ^~ l_data[010] ^~ l_data[009];
          137: l_xnor = l_data[136] ^~ l_data[115];
          138: l_xnor = l_data[137] ^~ l_data[136] ^~ l_data[130] ^~ l_data[129];
          139: l_xnor = l_data[138] ^~ l_data[135] ^~ l_data[133] ^~ l_data[130];
          140: l_xnor = l_data[139] ^~ l_data[110];
          141: l_xnor = l_data[140] ^~ l_data[139] ^~ l_data[109] ^~ l_data[108];
          142: l_xnor = l_data[141] ^~ l_data[120];
          143: l_xnor = l_data[142] ^~ l_data[141] ^~ l_data[122] ^~ l_data[121];
          144: l_xnor = l_data[143] ^~ l_data[142] ^~ l_data[074] ^~ l_data[073];
          145: l_xnor = l_data[144] ^~ l_data[092];
          146: l_xnor = l_data[145] ^~ l_data[144] ^~ l_data[086] ^~ l_data[085];
          147: l_xnor = l_data[146] ^~ l_data[145] ^~ l_data[109] ^~ l_data[108];
          148: l_xnor = l_data[147] ^~ l_data[120];
          149: l_xnor = l_data[148] ^~ l_data[147] ^~ l_data[039] ^~ l_data[038];
          150: l_xnor = l_data[149] ^~ l_data[096];
          151: l_xnor = l_data[150] ^~ l_data[147];
          152: l_xnor = l_data[151] ^~ l_data[150] ^~ l_data[086] ^~ l_data[085];
          153: l_xnor = l_data[152] ^~ l_data[151];
          154: l_xnor = l_data[153] ^~ l_data[151] ^~ l_data[026] ^~ l_data[024];
          155: l_xnor = l_data[154] ^~ l_data[153] ^~ l_data[123] ^~ l_data[122];
          156: l_xnor = l_data[155] ^~ l_data[154] ^~ l_data[040] ^~ l_data[039];
          157: l_xnor = l_data[156] ^~ l_data[155] ^~ l_data[130] ^~ l_data[129];
          158: l_xnor = l_data[157] ^~ l_data[156] ^~ l_data[131] ^~ l_data[130];
          159: l_xnor = l_data[158] ^~ l_data[127];
          160: l_xnor = l_data[159] ^~ l_data[158] ^~ l_data[141] ^~ l_data[140];
          161: l_xnor = l_data[160] ^~ l_data[142];
          162: l_xnor = l_data[161] ^~ l_data[160] ^~ l_data[074] ^~ l_data[073];
          163: l_xnor = l_data[162] ^~ l_data[161] ^~ l_data[103] ^~ l_data[102];
          164: l_xnor = l_data[163] ^~ l_data[162] ^~ l_data[150] ^~ l_data[149];
          165: l_xnor = l_data[164] ^~ l_data[163] ^~ l_data[134] ^~ l_data[133];
          166: l_xnor = l_data[165] ^~ l_data[163] ^~ l_data[127] ^~ l_data[126];
          167: l_xnor = l_data[166] ^~ l_data[160];
          168: l_xnor = l_data[167] ^~ l_data[165] ^~ l_data[152] ^~ l_data[150];
        endcase // case (LENGTH)
     end

   always@(posedge clk)
     begin
        if(rst) l_data = i_seed;
        else 
          begin
             l_data[LENGTH-1:1] = l_data[LENGTH-2:0];
             l_data[0] = l_xnor;
          end
     end

   assign o_data = l_data;
   
endmodule // lfsr
