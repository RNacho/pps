module qamtx
  #(
    parameter SYMBOL_NB,
    parameter DATA_NB,
    parameter DATA_NBF
    )(
      output [DATA_NB-1:0]  o_tx_data,
      input [SYMBOL_NB-1:0] i_tx_symbol,
      input                 rst,
      input                 clk
      );
   
endmodule // qamtx