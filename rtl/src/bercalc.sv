module bercalc
  #(
    parameter SYMBOL_NB,
    parameter DATA_NB,
    parameter DATA_NBF
    )(
      output [DATA_NB-1:0] o_ber,
      input  [SYMBOL_NB-1:0] i_tx_symbol,
      input  [SYMBOL_NB-1:0] i_rx_symbol,
      input  [SYMBOL_NB-1:0] i_symbol_mask,
      input  rst,
      input  clk
      );

endmodule // bercalc