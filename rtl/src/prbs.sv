module prbs
  #(
    parameter OUTPUT_NB,
    parameter LFSR_LENGTH
    )(
      output [OUTPUT_NB-1:0]  o_data,
      input [LFSR_LENGTH-1:0] i_seed,
      input                   rst,
      input                   clk
      );

   logic [LFSR_LENGTH-1:0]   l_lfsr;  
   
   lfsr #(
          .LENGTH(LFSR_LENGTH)
          ) 
   lfsr_prbs (
           .o_data(l_lfsr),
           .i_seed(i_seed),
           .rst(rst),
           .clk(clk)
           );

   assign o_data = l_lfsr[OUTPUT_NB-1:0];

endmodule // prbs