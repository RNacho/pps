module qamrx
  #(
    parameter SYMBOL_NB,
    parameter DATA_NB,
    parameter DATA_NBF
    )(
      output [SYMBOL_NB-1:0] o_rx_symbol,
      input [DATA_NB-1:0]    i_rx_data,
      input                  rst,
      input                  clk
      );
   
endmodule // qamrx