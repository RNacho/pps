module mask #(
              parameter NB
              )(
                output [NB-1:0] out,
                input  [NB-1:0] in,
                input  [NB-1:0] mask
                );
   
   assign out = in & mask;

endmodule // mask