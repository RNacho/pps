module fifo
  #(
    parameter NB,
    parameter SIZE
    )(
      output [NB-1:0] out,
      input [NB-1:0]  in,
      input           rst,
      input           clk
      );
   
   logic [NB-1:0]     l_buffer [0:SIZE-1];
   genvar             gen_i;

   always@(posedge clk) 
     begin
        if(rst) l_buffer[0] <= {NB{1'b0}};
        else l_buffer[0] <= in;
     end

   for(gen_i=1; gen_i < SIZE; gen_i++) begin : gen_buffer
      always@(posedge clk)
        begin
           if(rst) l_buffer[gen_i] <= {NB{1'b0}};
           else l_buffer[gen_i] <= l_buffer[gen_i-1];
        end
   end

   assign out = l_buffer[SIZE-1];
   
endmodule // fifo