module top
  #(
    parameter LFSR_LENGTH = 64,
    parameter MAX_SYMBOL_NB = 4, // *Max possible* symbol bits, gives (2**MAX)QAM
    parameter DATA_NBF = 16,
    parameter DATA_NB = MAX_SYMBOL_NB + DATA_NBF
    )(
      output [DATA_NB-1:0]      o_ber,
      input [LFSR_LENGTH-1:0]   i_random_seed,
      input [MAX_SYMBOL_NB-1:0] i_symbol_bits_mask,
      input [DATA_NB-1:0]       i_eq_beta, 
      input [DATA_NB-1:0]       i_sim_snr_db, 
      input                     rst,
      input                     clk
      );
   
   logic [MAX_SYMBOL_NB-1:0]    l_prbs_symbol, l_tx_symbol, l_rx_symbol, 
                                l_tx_symbol_compare;
   logic [DATA_NB-1:0]          l_tx_data;
   
   prbs #(
          .OUTPUT_NB(MAX_SYMBOL_NB),
          .LFSR_LENGTH(LFSR_LENGTH)
          ) 
   prbs_tx_symbol (
           .o_data(l_prbs_symbol),
           .i_seed(i_random_seed),
           .rst(rst),
           .clk(clk)
           );

   mask #(
          .NB(MAX_SYMBOL_NB)
          )
   mask_tx (
            .out(l_tx_symbol),
            .in(l_prbs_symbol),
            .mask(i_symbol_bits_mask)
            );
   
   qamtx #(
         .SYMBOL_NB(MAX_SYMBOL_NB),
         .DATA_NB(DATA_NB),
         .DATA_NBF(DATA_NBF)
         )
   u_qamtx (
              .o_tx_data(l_tx_data),
              .i_tx_symbol(l_tx_symbol),
              .rst(rst),
              .clk(clk)
              );

   qamrx #(
         .SYMBOL_NB(MAX_SYMBOL_NB),
         .DATA_NB(DATA_NB),
         .DATA_NBF(DATA_NBF)
           )
   u_qamrx (
              .i_rx_data(l_tx_data),
              .o_rx_symbol(l_rx_symbol),
              .rst(rst),
              .clk(clk)
              );

   fifo #(
            .NB(MAX_SYMBOL_NB),
            .SIZE(10)
            )
   fifo_tx_symbol(
                    .in(l_tx_symbol),
                    .out(l_tx_symbol_compare),
                    .rst(rst),
                    .clk(clk)
                    );

   bercalc #(
             .SYMBOL_NB(MAX_SYMBOL_NB),
             .DATA_NB(DATA_NB),
             .DATA_NBF(DATA_NBF)
             )
   u_bercalc (
              .o_ber(o_ber),
              .i_tx_symbol(l_tx_symbol_compare),
              .i_rx_symbol(l_rx_symbol),
              .i_symbol_mask(i_symbol_bits_mask),
              .rst(rst),
              .clk(clk)
              );
   
endmodule // top