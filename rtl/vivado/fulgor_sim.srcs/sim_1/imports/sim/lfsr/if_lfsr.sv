interface if_lfsr #(parameter LENGTH) (input bit clk);
   logic rst;
   logic [LENGTH-1:0] o_data;
   logic [LENGTH-1:0] i_seed;
endinterface // if_lfsr